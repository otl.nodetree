# Copyright (c) 2008-2009 by Florian Friesdorf
#
# GNU Affero General Public License (AGPL)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
"""
__author__ = "Florian Friesdorf <flo@chaoflow.net>"
__docformat__ = "plaintext"

from zope.interface import implements

from otl.nodetree.interfaces import INode
from otl.nodetree.interfaces import IParent


class Parent(object):
    """A parent to a node
    """
    implements(IParent)

    def __init__(self):
        self._childs = []

    def __getitem__(self, i):
        return self._childs[i]

    def __len__(self):
        return len(self._childs)

    def append(self, child):
        self._childs.append(child)
        child.__parent__ = self


class Node(Parent):
    """A node in an outliner node tree
    """
    implements(INode)

    __parent__ = None

    def __init__(self, content=u''):
        Parent.__init__(self)
        self.content = content
