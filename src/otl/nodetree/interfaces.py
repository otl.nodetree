# Copyright (c) 2008-2009 by Florian Friesdorf
#
# GNU Affero General Public License (AGPL)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
"""
__author__ = "Florian Friesdorf <flo@chaoflow.net>"
__docformat__ = "plaintext"

from zope.interface import Interface
from zope.schema import Object, Text

class IndentedToFar(Exception):
    """The maximum positive change of indentation is 1
    """

class IParent(Interface):
    """A parent to a node

    eventually could become IParent(ISequence)
    """
    def __getitem__(i):
        """get child number i
        """

    def __len__():
        """return number of childs, non-recursive
        """

    def append(child):
        """Append child to node, the node will be set as child's parent
        """


class INode(IParent):
    """A node in an outliner node tree
    """
    __parent__ = Object(
            title=u"Parent",
            description=u"The node's parent node",
            schema=IParent,
            )

    content = Text(
            title=u"Content",
            description=u"The node's textual content",
            )

class IPrefixedNode(INode):
    """A node with prefixed content to indicate its type.

    an iterim format used when parsing otl strings
    """

class IOneLineNode(INode):
    """A node consisting of a single line
    """

class IMultiLineNode(INode):
    """A node, that may consist of multiple lines
    """

class IWrapping(Interface):
    """marker for wrapping multline nodes
    """

class IPreformatted(Interface):
    """marker for preformatted multline nodes
    """

class IBodyText(IMultiLineNode, IWrapping):
    """node containing body text, i.e. wrapping text
    """

class IPreformattedBodyText(IMultiLineNode, IPreformatted):
    """node containing preformatted body text, i.e. non-wrapping text
    """

class ITable(IMultiLineNode, IPreformatted):
    """node containing a table
    """

class IUserDefinedText(IMultiLineNode, IWrapping):
    """node containing a user-defined wrapping text block
    """

class IUserDefinedPreformattedText(IMultiLineNode, IPreformatted):
    """node containing a user-defined preformatted text block
    """
