# Copyright (c) 2008-2009 by Florian Friesdorf
#
# GNU Affero General Public License (AGPL)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
"""
__author__ = "Florian Friesdorf <flo@chaoflow.net>"
__docformat__ = "plaintext"

from otl.nodetree.interfaces import IndentedToFar

def getparent(node, depthdiff=0):
    """find parent for a node relative depthdiff to node
    """
    # same depth as node, i.e. same parent
    if depthdiff == 0:
        return node.__parent__

    # one level deeper than node, i.e node is parent
    if depthdiff == 1:
        return node

    # more than one level deeper than node, i.e. error
    if depthdiff > 1:
        raise IndentedToFar

    # less deep than node, i.e. climb upwards
    parent = getparent(node.__parent__, depthdiff+1)
    return parent
