GetParent:
-----------

    >>> from otl.nodetree.utils import getparent

    >>> root = Mock()
    >>> node1 = Mock(__parent__=root)
    >>> node2 = Mock(__parent__=node1)
    >>> node3 = Mock(__parent__=node2)

    >>> getparent(node1) is root
    True

    >>> getparent(node1, 0) is root
    True

    >>> getparent(node1, 1) is node1
    True

    >>> getparent(node1, 2)
    Traceback (most recent call last):
    ...
    IndentedToFar

    >>> getparent(node3, -1) is node1
    True

    >>> getparent(node3, -2) is root
    True

    >>> getparent(node3, -3)
    Traceback (most recent call last):
    ...
    AttributeError: ...


Parent and Node:
----------------

    >>> from otl.nodetree.node import Node, Parent
    >>> parent = Parent()
    >>> len(parent)
    0
    >>> node1 = Node('node1')
    >>> node1.content is 'node1'
    True
    >>> node1.__parent__ is None
    True
    >>> node2 = Node('node2')
    >>> node21 = Node('node21')

    >>> parent.append(node1)
    >>> len(parent)
    1
    >>> node1.__parent__ is parent
    True
    >>> parent.append(node2)
    >>> len(parent)
    2
    >>> node2.append(node21)
    >>> parent[0] is node1
    True
    >>> parent[1] is node2
    True
    >>> parent[1][0] is node21
    True
